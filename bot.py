# Coded by github u/tandemdude
# https://github.com/tandemdude
import discord
from discord.ext import commands
import time
import logging
import configparser
import json

logging.basicConfig(level=logging.INFO)
# Sets up parser and reads the file containing the bot token
parser = configparser.ConfigParser()
parser.read('TOKEN.INI')

# List of all extensions to be loaded
extensions = ['cogs.OnReadyCog', 'cogs.FunCog', 'cogs.UptimeCog', 'cogs.OwnerCog', 'cogs.OverwatchCog', 'cogs.SnipeCog',
              'cogs.CogsCog', 'cogs.PingPongCog', 'cogs.GayCog', 'cogs.MinesweeperCog', 'libneko.extras.superuser',
              'cogs.MinecraftCog', 'cogs.ModerationCog', 'cogs.HangmanCog', 'cogs.ListenersCog', 'libneko.extras.help',
              'cogs.ConnectFourCog']

# Declares the bot prefix and token, taking values from files
prefix = '!e'
token = parser['DEFAULT']['token']


# Main function creates bot, loads extensions and runs the bot
def run_bot():
    bot = commands.Bot(command_prefix=prefix, owner_id=215061635574792192, case_insensitive=True)
    if len(extensions) != 0:
        for ext in extensions:
            bot.load_extension(ext)
    bot.run(token)


# Keeps the bot alive 
while True:
    run_bot()
    time.sleep(5)
