# Moderation extension made by github u/tandemdude
# https://github.com/tandemdude
import discord
from discord.ext import commands


class Moderation(commands.Cog):

	def __init__(self, bot):
		self.bot = bot
		self.dev_id = 215061635574792192

	@commands.command()
	@commands.has_permissions(kick_members=True)
	async def kick(self, ctx, member: discord.Member):
		try:
			await member.kick()
			await ctx.send(f'{member.display_name} was kicked.')
		except (discord.Forbidden, discord.NotFound, HTTPException):
			await ctx.send('The action could not be completed. Do I have permissions to kick?')

	@commands.command()
	@commands.has_permissions(ban_members=True)
	async def permban(self, ctx, member: discord.Member):
		try:
			await member.ban()
			await ctx.send(f'{member.display_name} was banned.')
		except (discord.Forbidden, discord.NotFound, HTTPException):
			await ctx.send('The action could not be completed. Do I have permissions to ban?')

	@commands.command()
	@commands.has_permissions(manage_messages=True)
	async def purge(self, ctx, param: int):
		"""Purges x messages from the channel, format !purge x"""
		count = min(param, 100)
		await ctx.message.delete()
		deleted = await ctx.channel.purge(limit=count)
		embed = discord.Embed(title=f'Deleted **{len(deleted)}** messages', description=f'<:Emi_Dab:595003798699769857> Requested by {ctx.author.display_name}', color=0x77dd77)
		await ctx.send(embed=embed)

	@commands.command()
	@commands.has_permissions(manage_messages=True)
	async def mute(self, ctx, member: discord.Member=None):
		if member is None:
			await ctx.send("What? Do you expect me to mute the air?")
		elif member == ctx.author:
			await ctx.send("You can't mute yourself duh")
		else:
			muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
			if muted_role is not None:
				try:
					await member.add_roles(muted_role, reason=f"{str(ctx.author)} muted {str(member)}")
					await ctx.send(f"{str(member)} was successfully muted <:Emi_Dab:595003798699769857>")
				except (Forbidden, HTTPException):
					await ctx.send("Oh dear looks like I wasn't able to do that <:Emi_Cry:595003787941380133>\nPing thommo if this persists.")
			else:
				await ctx.send("Looks like there isn't a role called `Muted` for me to give people <:Emi_Cry:595003787941380133>\nI guess you gotta make one.")

	@commands.command()
	@commands.has_permissions(manage_messages=True)
	async def unmute(self, ctx, member: discord.Member=None):
		if member is None:
			await ctx.send("Oh yeah let me just unmute nothing. Like that's going to work.")
		elif member == ctx.author:
			await ctx.send("Honestly what are you doing? You can't unmute yourself.")
		else:
			muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
			if muted_role is not None:
				if muted_role in member.roles:
					try:
						await member.add_roles(muted_role, reason=f"{str(ctx.author)} unmuted {str(member)}")
						await ctx.send(f"{str(member)} was successfully unmuted <:Emi_Dab:595003798699769857>")
					except (Forbidden, HTTPException):
						await ctx.send("Oh dear looks like I wasn't able to do that <:Emi_Cry:595003787941380133>\nPing thommo if this persists.")
				else:
					await ctx.send("I can't unmute someone who isn't muted duh")
			else:
				await ctx.send("Looks like there isn't a role called `Muted` for me to remove from people <:Emi_Cry:595003787941380133>\nI guess you gotta make one.")


def setup(bot):
	bot.add_cog(Moderation(bot))