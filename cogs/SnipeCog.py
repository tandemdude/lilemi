import discord
from discord.ext import commands


class SnipeCog(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot
		self.previous_deleted_msg = None

	@commands.Cog.listener()
	async def on_message_delete(self, message):
		if not message.author.bot:
			self.previous_deleted_msg = message

	@commands.command()
	async def snipe(self, ctx):
		if self.previous_deleted_msg is not None:
			await ctx.send(embed=discord.Embed(title=f"<#{self.previous_deleted_msg.channel.id}>", description=self.previous_deleted_msg.content, colour=0xec6761)
										.set_author(name=str(self.previous_deleted_msg.author), icon_url=self.previous_deleted_msg.author.avatar_url))
		else:
			await ctx.send("No messages could be found.")


def setup(bot):
	bot.add_cog(SnipeCog(bot))