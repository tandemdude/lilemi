# Minesweeper extension coded by github u/tandemdude
# https://github.com/tandemdude
# Special thanks to LunarCoffee#2553 for the help and advice (and a lil bit of code :P)
import discord
import random
from discord.ext import commands

words = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight'}


class Minesweeper(commands.Cog):

	def __init__(self, bot):
		self.bot = bot

	@commands.command(aliases=['minesweeper'])
	async def msp(self, ctx, size: int = 8, bombs: int = 10):

		# Not confirmed working
		def num_to_word(num):
			if num == -1:
				return 'bomb'
			else:
				return words[num]

		# Confirmed working
		def create_embed(board, bombs):
			emb = discord.Embed(title=':boom: Minesweeper!', description=board, colour=0xff0000)
			emb.set_footer(text=f'There are {bombs} mines to be found!')
			return emb

		# Confirmed working
		def create_board_str(board_array):
			board = ''
			for row in board_array:
				for item in row:
					if item == -1:
						board += f'||:bomb:|| '
					else:
						board += f'||:{num_to_word(item)}:|| '
				board += '\n'
			return board

		def filter_neighbours(neighbours, size):
			filtered_neighbours = []
			for neighbour in neighbours:
				if -1 not in neighbour and size not in neighbour:
					filtered_neighbours.append(neighbour)
			return filtered_neighbours

		def get_cell_neighbours(y, x):
			neighbours = []
			for i in range(y-1, y+2):
				for j in range(x-1, x+2):
					neighbours.append((i, j))
			return neighbours

		def count_bombs(board_array, size):
			board = board_array
			for y in range(size):
				for x in range(size):
					if board[y][x] != -1:
						cell_neighbours = filter_neighbours(get_cell_neighbours(y, x), size)
						for neighbour in cell_neighbours:
							if board[neighbour[0]][neighbour[1]] == -1:
								board[y][x] += 1
			return board

		# Confirmed working
		def place_bomb(board_array, size):
			y, x = random.randint(0, size-1), random.randint(0, size-1)
			if board_array[y][x] == -1:
				place_bomb(board_array, size)
			else:
				board_array[y][x] = -1

		# Confirmed working
		def create_board_array(size):
			board_array = []
			for i in range(size):
				temp = []
				for j in range(size):
					temp.append(0)
				board_array.append(temp)
			return board_array

		if bombs < (size*size):
			new_board = create_board_array(size)
			for _ in range(bombs):
				place_bomb(new_board, size)
			completed_board = count_bombs(new_board, size)
			board_str = create_board_str(completed_board)
			b = create_embed(board_str, bombs)
			await ctx.send(embed=b)
		else:
			await ctx.send(':x: I can\'t add that amount of mines! **¯\_(ツ)_/¯**')


def setup(bot):
	bot.add_cog(Minesweeper(bot))
