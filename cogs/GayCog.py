import discord
import random
from discord.ext import commands


class Gay(commands.Cog):

	def __init__(self, bot):
		self.bot = bot

	@commands.command()
	async def gay(self, ctx, member=None, option='c'):

		def create_embed(user, num, emoji):
			embed = discord.Embed(title='Gay Machine:', description=f'{user.mention} is {num}% gay {emoji}', colour=0x42f4d4)
			return embed

		if option == 'r':
			percent_gay = random.randint(0, 100)
			if member:
				mentioned_user = ctx.message.mentions
				if percent_gay >= 50:
					emoji = ':gay_pride_flag:'
					await ctx.send(embed=create_embed(mentioned_user[0], percent_gay, emoji))
				else:
					emoji = '😄'
					await ctx.send(embed=create_embed(mentioned_user[0], percent_gay, emoji))

			else:
				if percent_gay >= 50:
					emoji = ':gay_pride_flag:'
					await ctx.send(embed=create_embed(ctx.author, percent_gay, emoji))
				else:
					emoji = '😄'
					await ctx.send(embed=create_embed(ctx.author, percent_gay, emoji))
		else:
			if member:
				mentioned_user = ctx.message.mentions
				mentioned_user_id = mentioned_user[0].id
				percent_gay = int(str(mentioned_user_id)[:2])
				if percent_gay >= 50:
					emoji = ':gay_pride_flag:'
					await ctx.send(embed=create_embed(mentioned_user[0], percent_gay, emoji))
				else:
					emoji = '😄'
					await ctx.send(embed=create_embed(mentioned_user[0], percent_gay, emoji))
			else:
				user_id = ctx.author.id
				percent_gay = int(str(user_id)[:2])
				if percent_gay >= 50:
					emoji = ':gay_pride_flag:'
					await ctx.send(embed=create_embed(ctx.author, percent_gay, emoji))
				else:
					emoji = '😄'
					await ctx.send(embed=create_embed(ctx.author, percent_gay, emoji))


def setup(bot):
	bot.add_cog(Gay(bot))