# Commands extension made by github u/tandemdude
# https://github.com/tandemdude
from discord.ext import commands
import discord
import psutil
import random
import os

possible_responses = ['It is certain', 'It is decidely so', 'Without a doubt', 'Yes - definitely',
                      'you may rely on it', 'As I see it, yes', 'Most likely', 'Outlook good',
                      'Yes', 'Signs point to yes', 'Reply hazy, try again', 'Ask again later',
                      'Better not tell you now', 'Cannot predict now', 'Concentrate and ask again',
                      "Don't count on it", 'My reply is no', 'My sources say no', 'Outlook not so good',
                      'Very doubtful']


class FunCommands(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='8ball')
    async def eightball(self, ctx):
        """Returns an answer like a magic 8 ball"""
        await ctx.send(f'🎱 {random.choice(possible_responses)}')

    @commands.command()
    async def rps(self, ctx, choice=None):
        """Plays rock, paper, scissors vs the bot"""
        rps_options = ['rock', 'paper', 'scissors']
        if choice != 'rock' and choice != 'paper' and choice != 'scissors':
            await ctx.send('You need to specify a choice! Options are: `rock`, `paper` or `scissors`.')
        else:
            bot_choice = random.choice(rps_options)
            if (choice=='rock' and bot_choice=='rock') or (choice=='paper' and bot_choice=='paper') or (choice=='scissors' and bot_choice=='scissors'):
                await ctx.send(f'Draw! We both picked {choice}')
            elif (choice=='rock' and bot_choice=='scissors') or (choice=='paper' and bot_choice=='rock') or (choice=='scissors' and bot_choice=='paper'):
                await ctx.send(f'You win! I picked {bot_choice}')
            elif (choice=='rock' and bot_choice=='paper') or (choice=='paper' and bot_choice=='scissors') or (choice=='scissors' and bot_choice=='rock'):
                await ctx.send(f'I win! My choice was {bot_choice}')
            else:
                await ctx.send(':x: Oh No! You found an error. Please dm my creator, thomm.o#0001, with details. :x:')

    @commands.command()
    async def toss(self, ctx):
        """Tosses a fair coin"""
        coin_options = ['Heads', 'Tails']
        await ctx.send(f'You flipped a coin. It landed {random.choice(coin_options)}')

    @commands.command()
    async def len(self, ctx, *, arg):
        """Returns the length of the string argument"""
        await ctx.send(f'Your text is `{len(arg)}` characters long!')

    @commands.command()
    async def mock(self, ctx, *, arg):
        """Mocks any message send as the command argument"""
        await ctx.send(''.join([arg[x].upper() if random.randint(0, 1) else arg[x].lower() for x in range(len(arg))]))

    @commands.command()
    async def info(self, ctx):
        """Views general bot info"""
        embed = discord.Embed(title='Lil-Emi', colour=0x7289DA)
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.add_field(name='Version:', value='0.0.1 - Bacon Roll', inline=False)
        embed.add_field(name='Library:', value='[discord.py](https://discordpy.readthedocs.io/en/latest/)', inline=False)
        embed.add_field(name='Stats:', value=f'Guilds: {len(self.bot.guilds)}\nUsers: {len([*self.bot.get_all_members()])}', inline=False)
        embed.add_field(name='Source:', value='[Gitlab](https://gitlab.com/tandemdude/lilemi)', inline=False)
        embed.add_field(name='Memory:', value=f'{psutil.virtual_memory().percent}% load')
        embed.set_footer(text='Coded by the one and only thomm.o#0001')
        await ctx.send(embed=embed)

    @commands.command()
    async def git(self, ctx):
        """Links to source code"""
        await ctx.send('Oh you wanna look at my source do ya? <:Emi_What:595314682949140485>\nWell here ya go: https://gitlab.com/tandemdude/lilemi')

    @commands.command()
    async def peepee(self, ctx, member: discord.Member=None):
        """Peepee size visualiser"""
        length = random.randint(0, 10)
        user = member if member else ctx.author
        await ctx.send(embed=discord.Embed(title='Peepee size visualiser:', description=f"{user.mention}'s peepee\n8{'='*length}D", colour=random.randint(0, 16777215)))   

    @commands.command()
    async def avatar(self, ctx, user: discord.User=None):
        """Get the avatar of the specified user"""
        u = user if user else ctx.author
        emb = discord.Embed(title=f'{str(u)}\'s avatar:', colour=0xfe6b64)
        emb.set_image(url=u.avatar_url)
        await ctx.send(embed=emb)

    @commands.command()
    async def tias(self, ctx):
        """Try it and see"""
        await ctx.send(embed=discord.Embed(title=None, colour=0xfe6b64)
                            .set_image(url="https://i.imgur.com/lMlFpsA.png")
                        )


def setup(bot):
    bot.add_cog(FunCommands(bot))
