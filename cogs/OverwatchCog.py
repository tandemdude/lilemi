import discord
import aiohttp
from discord.ext import commands


class OverwatchUtils(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot

	@commands.group(invoke_without_command=True, aliases=['ow'])
	async def overwatch(self, ctx):
		emb = discord.Embed(title='Available Subcommands:', colour=0xfb9c26)
		emb.add_field(name='stats [username] [battletag] [region]', value='Username = battle.net username, defaults to `Emi`\nBattletag = battle.net battletag, defaults to `11232`\n Region = game region, options: `us`, `eu`, `asia`, defaults to `us`', inline=False)
		emb.set_footer(text='!eoverwatch <subcommand> [params]')
		emb.set_thumbnail(url='https://i.imgur.com/JYG4IC9.png')
		await ctx.send(embed=emb)

	@overwatch.command()
	async def stats(self, ctx, name: str='Emi', tag: str='11232', region: str='us'):
		url = f'https://ow-api.com/v1/stats/pc/{region}/{name}-{tag}/profile'
		try:
			async with aiohttp.ClientSession() as session:
				resp = await session.get(url)
				data = await resp.json()
			emb = discord.Embed(title=f'Stats for {name}#{tag}', colour=0xfb9c26)
			emb.set_thumbnail(url=data['ratingIcon'])
			emb.set_author(name=f'{name}#{tag}', icon_url=data['icon'])
			emb.add_field(name='Level:', value=f"{(int(data['prestige'])*100)+int(data['level'])}", inline=False)
			emb.add_field(name='*SR:', value=f"{data['rating']}", inline=False)
			emb.add_field(name='*Games Won:', value=f"{data['gamesWon']}", inline=False)
			emb.add_field(name='Private', value=f"{data['private']}", inline=False)
			emb.set_footer(text='*Some data may be incorrect if profile is private.')
			await ctx.send(embed=emb)
		except (KeyError, ValueError, AttributeError):
			await ctx.send(embed=discord.Embed(title='Oh no! Something went wrong <:Emi_Cry:595003787941380133>', description=":no_entry_sign: Error 404: Profile not found.", colour=0xfb9c26))


def setup(bot):
	bot.add_cog(OverwatchUtils(bot))