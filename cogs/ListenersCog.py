from discord.ext import commands
import discord


class Listeners(commands.Cog):
	
	def __init__(self, bot):
		self.bot = bot
		self.emi_id = 287635909422350336

	@commands.Cog.listener()
	async def on_message(self, message):
		emi = self.bot.get_user(self.emi_id)
		if self.bot.user in message.mentions:
			await message.channel.send('Hi there! <:Emi_Hearts:595273767656357899>\nMy command prefix is `!e`, you can view available commands with `!ehelp`.')
		elif emi in message.mentions:
			await message.add_reaction('<:Emi_Ping:595266901693956116>')

	@commands.Cog.listener()
	async def on_command_error(self, ctx, error):
		error = error.__cause__ or error
		if isinstance(error, commands.errors.CommandNotFound):
			e = discord.Embed(title="<:Emi_What:595314682949140485> Woah it seems that I don't know what you wanted me to do <:Emi_What:595314682949140485>", description="Maybe look at my help menu if you're stuck?", colour=0xec6761)
			e.set_footer(text='!ehelp')
			await ctx.send(embed=e)
		elif isinstance(error, commands.errors.MissingPermissions):
			await ctx.send(embed=discord.Embed(title='<:Emi_Knife:595652337192927242> Nuh uh', description='You don\'t have the right permissions for that buddy', colour=0xec6761))

	@commands.Cog.listener()
	async def on_member_remove(member):
		channel = member.guild.get_channel(539908361442033675)
		e = discord.Embed(title=f'`{str(member)}` got bent', colour=0xec6761)
		e.set_footer('Goodbye...')
		await channel.send(embed=e)


def setup(bot):
	bot.add_cog(Listeners(bot))