# On ready listener extension coded by github u/tandemdude
# https://github.com/tandemdude
import discord
import json
import asyncio
from discord.ext import commands


class OnReady(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.prefix = '!e'

    @commands.Cog.listener()
    async def on_ready(self):
        print('The bot is ready!')
        while True:
            await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='with Emily <3'))
            await asyncio.sleep(60)
            await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='with Eso'))
            await asyncio.sleep(60)
            await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name='Thommo code'))
            await asyncio.sleep(60)
            await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='with Baby-Emi'))
            await asyncio.sleep(60)


def setup(bot):
    bot.add_cog(OnReady(bot))