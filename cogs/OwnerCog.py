# Bot owner commands extension made by github u/tandemdude
# https://github.com/tandemdude
from discord.ext import commands
import discord
import os
import psutil
import re


class Owner(commands.Cog):
	def __init__(self, bot):
		self.bot = bot

	@commands.command()
	@commands.is_owner()
	async def bedtime(self, ctx):
		"""Disconnects the bot from discord and stops the script"""
		await ctx.send('I\'m going to sleep. Baii <:Emi_Waves:595003762880544810>')
		await self.bot.logout()

	@commands.command()
	@commands.is_owner()
	async def temp(self, ctx):
		"""Gets the current CPU temp of the raspberry pi"""
		temp = os.popen('vcgencmd measure_temp').read()
		temp_stripped = re.findall(r'(\d+(?:\.\d+)?)', temp)
		current_temp = temp_stripped[0]
		await ctx.send(embed=discord.Embed(title=':thermometer: Current operating temperature:', description=f'{current_temp}°C', colour=0xff0000))

	@commands.command(aliases=['sys', 'sysinfo'])
	@commands.is_owner()
	async def system(self, ctx):
		"""Views general system info"""
		cpu_usage = psutil.cpu_percent(percpu=True)
		ram_usage = psutil.virtual_memory().percent
		swap_usage =psutil.swap_memory().percent
		embed = discord.Embed(title='System Information', description=f'```CPU 0: {cpu_usage[0]}%\nCPU 1: {cpu_usage[1]}%\nCPU 2: {cpu_usage[2]}%\nCPU 3: {cpu_usage[3]}%\n===============\nRAM: {ram_usage}%\nSWAP: {swap_usage}%```', colour=0x7289DA)
		embed.set_thumbnail(url='https://www.raspberrypi.org/wp-content/uploads/2011/10/Raspi-PGB001.png')
		await ctx.send(embed=embed)


def setup(bot):
	bot.add_cog(Owner(bot))